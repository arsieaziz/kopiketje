$(window).on('load',function(e) {
	$('.preloader-wrapper').hide();
});

$(document).ready(function(){

	var width = $(window).width();

	if(width > 768){
		$('body').niceScroll({
			cursorborder: '1px solid transparent',
			scrollspeed: 200,
	    	mousescrollstep: 50,
	    	horizrailenabled:false,
	    	zindex:'99999999'
		});
	}

	AOS.init({
	  duration: 1500
	});

	$('#lightgallery').lightGallery({
		showThumbByDefault: false
	});

	$('.navTrigger').click(function(){
		$(this).toggleClass('active');
		$('header').find('.menu').toggleClass('mobile-active');
		$('header').toggleClass('active-head');
		$('body').toggleClass('overflow-y-hidden');
		if($(this).attr('data-click-state') == 1) {
			$(this).attr('data-click-state', 0);
			if(width > 768){
				$('body').niceScroll({cursorborder: '1px solid transparent',scrollspeed: 200,mousescrollstep: 50});
			}
		} else {
			$('body').niceScroll().remove();
			$(this).attr('data-click-state', 1);
		}
	});

	if (width < 991.98){
		$('.menu').find('ul > li').has('ul').addClass('child');
		$('.menu').find('ul > li > ul > li').has('ul > li').removeClass('child');
		$('.menu').find('ul > li > ul > li').has('ul > li').addClass('child2');
	}else{
		$('.menu').find('ul > li > ul > li').has('ul > li').addClass('child');
	}
	$('.menu').find('ul > li').has('ul').ready(function(){
		if (width < 991.98){
			$('.menu').find('ul > .child > a').removeAttr('href');
		}
	});
	$('.menu').find('ul > li > ul > li').has('ul').ready(function(){
		if (width < 991.98){
			$('.menu').find('ul > li > ul > .child > a').removeAttr('href');
			$('.menu').find('ul > li > ul > .child2 > a').removeAttr('href');
		}
	});
	$('.menu').find('ul > li').has('ul').find('a').click(function(){
		$(this).next().toggleClass('active');
		$(this).parent().toggleClass('changed');
	});
	$('.menu').find('ul > li > ul > li').has('ul').find('a').click(function(){
		$(this).next().toggleClass('active-child');
	});

	var scrollLink = $('.scroll');
  
	// Smooth scrolling
	scrollLink.click(function(e) {
		e.preventDefault();
		$('body,html').animate({
			scrollTop: $(this.hash).offset().top
		}, 1000 );
	});

	var heightSc = $(window).height();
	
	// Active link switching
	// $(window).scroll(function() {
	// 	var scrollbarLocation = $(this).scrollTop();
	// 	scrollLink.each(function() {
	// 		var sectionOffset = $(this.hash).offset().top - 20;
	// 		if ( sectionOffset <= scrollbarLocation ) {
	// 			$(this).parent().addClass('active');
	// 			$(this).parent().siblings().removeClass('active');
	// 		}
	// 	});
	// 	if($(this).scrollTop() > (heightSc/4) && $(this).scrollTop() < (heightSc/2)){
	// 		$('header').addClass('pre-float');
	// 	}else if($(this).scrollTop() < (heightSc/4)){
	// 		$('header').removeClass('pre-float');
	// 		$('header').removeClass('float-menu');
	// 	}else if($(this).scrollTop() > (heightSc/2)){
	// 		$('header').addClass('float-menu');
	// 	}else{
	// 		$('header').removeClass('float-menu');
	// 	}
	// });

	$('h1.count').one('click',function(){
		$(this).each(function () {
			var $this = $(this);
			jQuery({ Counter: 0 }).animate({ Counter: $this.text() }, {
				duration: 1800,
				easing: 'swing',
				step: function () {
					$this.text(Math.ceil(this.Counter));
				}
			});
		});
	});
	$(window).scroll(function() {
		var scrollbarLocation = $(this).scrollTop();
		if(scrollbarLocation >= (heightSc/2)){
			$('h1.count').trigger('click');
		}
	});
	$('.slideshow').owlCarousel({
	    loop:true,
	    margin:0,
	    nav:true,
        items:1,
        dots:false,
        autoplay:true,
        autoplayTimeout:5000,
    	touchDrag:false,
    	mouseDrag:false
	});
	$('.menu-slide').owlCarousel({
	    loop:true,
	    margin:0,
	    nav:true,
        items:1,
        dots:false,
        autoplay:true,
        autoplayTimeout:5000,
    	touchDrag:false,
    	mouseDrag:false
	});
	$('.store-slide').owlCarousel({
	    loop:true,
	    margin:0,
	    nav:true,
        items:1,
        dots:false,
        autoplay:true,
        autoplayTimeout:5000,
    	touchDrag:false,
    	mouseDrag:false
	});

    var _overlay = document.getElementById('overlay');
    var _clientY = null; // remember Y position on touch start

    _overlay.addEventListener('touchstart', function (event) {
        if (event.targetTouches.length === 1) {
            // detect single touch
            _clientY = event.targetTouches[0].clientY;
        }
    }, false);

    _overlay.addEventListener('touchmove', function (event) {
        if (event.targetTouches.length === 1) {
            // detect single touch
            disableRubberBand(event);
        }
    }, false);

    function disableRubberBand(event) {
        var clientY = event.targetTouches[0].clientY - _clientY;

        if (_overlay.scrollTop === 0 && clientY > 0) {
            // element is at the top of its scroll
            event.preventDefault();
        }

        if (isOverlayTotallyScrolled() && clientY < 0) {
            //element is at the top of its scroll
            event.preventDefault();
        }
    }

    function isOverlayTotallyScrolled() {
        // https://developer.mozilla.org/en-US/docs/Web/API/Element/scrollHeight#Problems_and_solutions
        return _overlay.scrollHeight - _overlay.scrollTop <= _overlay.clientHeight;
    }

});